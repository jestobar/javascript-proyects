import React from 'react';

const HolaMundo =() =>{
    const Hello='Hola Mundo';
    const istrue=false;
    return(
        <div className="Hola Mundo">
            <h1>{Hello}</h1>
            <h2>Curso Basico de React</h2>
            <img src="https://arepa.s3.amazonaws.com/react.png" alt="React"  ></img>
            {istrue ? <h4>Esto es verdadero</h4>: <h5>Soy falso</h5>}
            {istrue && <h4>Soy verdadero</h4>}
        </div>

    );
};

export default HolaMundo;